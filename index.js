/*Create a variable port and assign it with the value of 3000.
Create a server using the createServer method that will listen in to the port provided above.
Console log in the terminal a message when the server is successfully running.
Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
- Access the login route to test if it’s working as intended.
- Create a condition for any other routes that will return an error message.
- Access any other route to test if it’s working as intended.*/

const http = require("http");
const port = 3000;

const server = http.createServer((request, response) =>{
 
  if(request.url == "/login"){
    response.writeHead(200, {"Content-Type": "text/plain"})
    response.end("Congratulations, You have successfully login!")

  } else {
    response.writeHead(404, {"Content-Type": "text/plain"})
    response.end("Error: Page Not Found")
  }
});

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});